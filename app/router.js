import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  //this.route('home');
  this.resource('home', function() {

  });
  this.route('items');
  this.route('about');
  this.route('clock');
});

export default Router;
